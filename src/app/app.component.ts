import {Component, OnInit} from '@angular/core';
import {Survey} from "../types/Survey";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  statuses: string[] = ['All', 'Active', 'Completed'];
  categories: string[] = ['Development', 'Workplace', 'Hardware'];
  filteredList: Survey[];

  status = 'status';
  category = "category";

  categoryFilter ="";
  CfilteredList = [];
  statusFilter = "";
  SfilteredList = []
  surveyList: Survey[] = [
    {
      title: "Designer Survey",
      category: "Workplace",
      status: "Active",
      label: "New Framework",
    },
    {
      title: "Developer Survey",
      category: "Development",
      status: "Active",
      label: "Education",
    },
    {
      title: "Backend Survey",
      category: "Hardware",
      status: "Completed",
      label: "Personal",
    }
  ]

  ngOnInit() {
    this.filteredList = this.surveyList

  }

  onFilterSelected(filter: string, type: string) {
    this.filteredList = this.surveyList;
    if (type==="status") {
      this.statusFiltered(filter);
    } else {
      this.categoryFiltered(filter);
    }
  }

  statusFiltered(filter){
    if(this.categoryFilter){
      this.filteredList = this.filteredList.filter((survey:Survey)=>{
          return survey.category === this.categoryFilter;
      })
    }
    this.filteredList = this.filteredList.filter((survey:Survey)=>{
      if (filter==="All") {
        return true;
      } else {
      return survey.status === filter;
      }
    })
    this.statusFilter = filter;
  }

  categoryFiltered(filter){
    if(this.statusFilter){
      this.filteredList = this.filteredList.filter((survey:Survey)=>{
        if (this.statusFilter==="All") {
          return true;
        } else {
          return survey.status === this.statusFilter;
        }
      })
    }
    if (filter !== this.categoryFilter){
      this.filteredList = this.filteredList.filter((survey:Survey)=>{
        return survey.category === filter;
    })
    }
    
    if (filter === this.categoryFilter) {
      this.categoryFilter = "";
    } else {
      this.categoryFilter = filter;
    }
  }
}
